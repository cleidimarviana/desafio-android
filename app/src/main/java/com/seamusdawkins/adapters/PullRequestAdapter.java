/*
*    The MIT License (MIT)
*
*   Copyright (c) 2016 Cleidimar Viana (cleidimarviana@gmail.com)
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:
*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*/
package com.seamusdawkins.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.seamusdawkins.R;
import com.seamusdawkins.models.PullRequest;
import com.seamusdawkins.models.PullRequestResult;
import com.seamusdawkins.utils.AndroidUtils;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.ArrayList;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private Boolean empty;
    private PullRequestResult collection;

    private Activity context;
    int pos;
    boolean loading;

    // Adapter's Constructor
    public PullRequestAdapter(Activity context, ArrayList<PullRequest> designs, Boolean empty, int pos) {
        this.context = context;
        this.empty = empty;
        this.pos = pos;
        this.collection = new PullRequestResult();
        this.collection.ar = designs;
    }

    public void Update(Boolean empty, int pos) {

        this.empty = empty;
        this.pos = pos;
        notifyDataSetChanged();
    }

    public void UpdateArraySearch(ArrayList<PullRequest> designs) {
        this.collection = new PullRequestResult();
        this.collection.ar = designs;
    }
    public void setLoading(boolean loading) {

        this.loading = loading;
    }

    // Create new views. This is invoked by the layout manager.
    @Override
    public PullRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view by inflating the row item xml.

        View v;
        if (viewType == 0) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_empty, parent, false);
            //Toast.makeText(context.getApplicationContext(), "" + context.getString(R.string.msg_result_error), Toast.LENGTH_SHORT).show();
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pull_request, parent, false);
        }
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int positionT) {

        if (this.empty) {
            holder.tvName.setText(context.getResources().getString(R.string.error_list_empty));
        } else {

            holder.tvName.setText(Html.fromHtml(collection.ar.get(positionT).getTitle()));
            holder.tvDescription.setText(Html.fromHtml(collection.ar.get(positionT).getBody()));

            if (!collection.ar.get(positionT).getUserImage().equals("")) {
                Picasso.with(context)
                        .load(collection.ar.get(positionT).getUserImage())
                        .placeholder(R.mipmap.github)
                        .tag(context)
                        .into(holder.ivUserImage);
            }

            holder.tvUserName.setText(Html.fromHtml(collection.ar.get(positionT).getUserLogin()));
            try {
                holder.tvDate.setText(""+ AndroidUtils.parseDateISO8601(collection.ar.get(positionT).getCreatedAt()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int color = context.getResources().getColor(R.color.sd_gray);
            holder.ivDate.setColorFilter(color);


            holder.toggleButtonDrawer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(collection.ar.get(positionT).getHtmlURL()));
                    context.startActivity(browserIntent);

                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && this.empty)

            return 0;
        return 1;
    }

    @Override
    public int getItemCount() {
        return collection.ar == null ? 0 : collection.ar.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        protected TextView tvName;
        protected TextView tvDescription;
        protected ImageView ivUserImage;
        protected TextView tvUserName;
        protected ToggleButton toggleButtonDrawer;
        protected TextView tvDate;
        protected ImageView ivDate;

        ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvDescription = (TextView) v.findViewById(R.id.tvDescription);
            ivUserImage = (ImageView) v.findViewById(R.id.ivUserImage);
            tvUserName = (TextView) v.findViewById(R.id.tvUserName);
            toggleButtonDrawer = (ToggleButton) v.findViewById(R.id.toggleButtonDrawer);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            ivDate = (ImageView) v.findViewById(R.id.ivDate);

        }
    }



}
