package com.seamusdawkins.utils;

/**
 * Created by fahrenheit on 02/12/15.
 */
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public abstract class HttpRequest {

    public static String lineEnd = "\r\n";
    public static String twoHyphens = "--";
    public static String boundary = "*****";
    public static String authorization = "Authorization";
    private final SharedPreferences sharedPreferences;

    protected boolean hasToken = false;
    protected boolean hasBody = false;
    private static final String TAG_ACTION = "action";
    private static final String TAG_STATUS = "status";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_RESPONSE = "response";
    private static final String TAG_DURATION = "duration";
    public Activity act;

    public static enum METODO {
        POST, GET, PUT, DELETE
    }

    protected String url;
    protected METODO metodo;

    private HttpRequest(String url, METODO metodo,
                        boolean hasToken, Activity act) {
        this.url = url;
        this.metodo = metodo;
        this.hasToken = hasToken;
        this.act = act;

        sharedPreferences = act.getSharedPreferences("VALUES", Context.MODE_PRIVATE);

        if (metodo != METODO.GET)
            this.hasBody = true;
    }

    public HttpRequest(String url, METODO metodo, Activity act) {
        this(url, metodo, false, act);
    }

    public String getUrl() throws UnsupportedEncodingException {
        if (this.metodo == METODO.GET)
            return this.url + "?" + getQuery();
        else
            return this.url;
    }

    protected ArrayList<NameValuePair> params;
    private String response;

    protected String getQuery() throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        if (params == null || params.size() == 0)
            return "";

        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public void setParam(String key, String value) {
        if (params == null)
            params = new ArrayList<NameValuePair>();

        for (int i = 0; i < params.size(); i++) {
            if (params.get(i).getName() == key) {
                params.remove(i);
                return;
            }
        }

        params.add(new BasicNameValuePair(key, value));
    }

    protected abstract void contentType(HttpURLConnection conexao);

    protected abstract void parametros(HttpURLConnection conexao)
            throws IOException;

    public String makeRequest() {
        String resposta = new String();

        URL url = null;
        HttpURLConnection conn = null;


        try {
            System.setProperty("http.keepAlive", "false");
            url = new URL(getUrl());
            conn = (HttpURLConnection) url.openConnection();

            // Defaults
            conn.setReadTimeout(30000);
            conn.setConnectTimeout(30000);
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setDoInput(true);
            conn.setDoOutput(this.hasBody);

            //DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            //BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));

            // Metodo
            conn.setRequestMethod(this.metodo.toString());

            if (this.hasBody) {
                this.contentType(conn);
                parametros(conn);
            }

            conn.connect();

            int stauscode = conn.getResponseCode();

            if(stauscode == 200){
                resposta = conn.getResponseMessage().toString();

                // Resposta do servidor
                resposta = getStringFromInputStream(new BufferedInputStream(
                        conn.getInputStream()));

            }else{

                resposta = String.valueOf(stauscode);

            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return resposta;
    }

    // convert InputStream to String
    public static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            while ((line = br.readLine()) != null) {
                sb.append(line);
                if (!br.ready()) {
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    public static String NomeArquivo(String sufix) {
        return sufix + Long.toString(System.currentTimeMillis());
    }

}