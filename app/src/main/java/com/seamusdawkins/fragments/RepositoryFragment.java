/*
*    The MIT License (MIT)
*
*   Copyright (c) 2016 Cleidimar Viana (cleidimarviana@gmail.com)
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:
*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*/
package com.seamusdawkins.fragments;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.seamusdawkins.R;
import com.seamusdawkins.adapters.RepositoryAdapter;
import com.seamusdawkins.models.Repository;
import com.seamusdawkins.utils.AndroidUtils;
import com.seamusdawkins.utils.Consts;
import com.seamusdawkins.utils.HttpHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class RepositoryFragment extends Fragment {

    private static String TAG = RepositoryFragment.class.getSimpleName();

    private RepositoryAdapter adapter;

    private String urlPost;
    private JSONObject jsonObjPoints;

    private boolean broadcast;
    private AsyncCampaignParseJson myTask;

    private ArrayList<Repository> ar;
    private boolean loadMore = true;
    private boolean ultimoEstadoConectado = true;
    private Boolean error = false;
    private boolean empty = false;
    private int pos;
    private RecyclerView recyclerView;

    private LinearLayoutManager linearlayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;

    private boolean carregando = false;
    private IntentFilter filters;
    private int recyclerViewPaddingTop;

    private static int currentOPage = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_campaign, container, false);

        initUI(view);           // mapping xml with java
        currentOPage = 1;

        filters = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

        // check connection with internet
        if(AndroidUtils.isConnected(getActivity())) {
            Log.wtf(TAG,"Connection");
            recyclerViewDevelop();  // setting recycleview
            swipeToRefresh();       // setup swipe to refresh
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_no_connection), Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
            recyclerViewDevelop();  // setting recycleview
            swipeToRefresh();       // setup swipe to refresh
        }

        return view;
    }

    /**
     * This method maps xml interface with java.
     */
    public void initUI(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewDevelop);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void swipeToRefresh() {
        int start = convertToPx(0), end = recyclerViewPaddingTop + convertToPx(16);
        swipeRefreshLayout.setProgressViewOffset(true, start, end);
        TypedValue typedValueColorPrimary = new TypedValue();
        TypedValue typedValueColorAccent = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.colorPrimary, typedValueColorPrimary, true);
        getActivity().getTheme().resolveAttribute(R.attr.colorAccent, typedValueColorAccent, true);
        final int colorPrimary = typedValueColorPrimary.data, colorAccent = typedValueColorAccent.data;
        swipeRefreshLayout.setColorSchemeColors(colorPrimary, colorAccent);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                error = false;
                empty = false;
                myTask.cancel(true);
                ar.clear();
                currentOPage = 1;
                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.INVISIBLE);
                mountUrl();
            }
        });
    }

    public int convertToPx(int dp) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (dp * scale + 0.5f);
    }


    /**
     * This method adjust url
     */
    private void mountUrl() {

        urlPost = Consts.URL_SERVER_DOMAIN + "search/repositories?q=language:Java&sort=stars&page="+currentOPage;
        myTask = new AsyncCampaignParseJson();
        myTask.execute(urlPost);
    }


    public class AsyncCampaignParseJson extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            adapter.setLoading(true);
            carregando = true;

        }

        // get JSON Object
        @Override
        protected String doInBackground(String... url) {

            urlPost = url[0];
            try {

                jsonObjPoints = HttpHelper.doGet(getActivity(), urlPost, "UTF-8", "");

                if (broadcast) {
                    ar.clear();
                    broadcast = false;
                    adapter.UpdateArraySearch(ar);
                }

                if (ar == null) {
                    ar = new ArrayList<Repository>();
                    adapter.UpdateArraySearch(ar);

                    if (currentOPage == 1) {
                        ar.clear();
                    }
                }

                assert jsonObjPoints != null;
                if (jsonObjPoints.length() != 0) {
                    JSONArray jsonArrPoints = jsonObjPoints.getJSONArray(Consts.ALL_ITENS);
                    for (int i = 0; i < jsonArrPoints.length(); i++) {

                        Repository srm = new Repository();

                        srm.setId(jsonArrPoints.getJSONObject(i).getInt("id"));
                        srm.setName(Html.fromHtml(jsonArrPoints.getJSONObject(i).getString("name")).toString());
                        srm.setDescription(Html.fromHtml(jsonArrPoints.getJSONObject(i).getString("description")).toString());
                        srm.setForks(jsonArrPoints.getJSONObject(i).getInt("forks_count"));
                        srm.setStars(jsonArrPoints.getJSONObject(i).getInt("stargazers_count"));

                        JSONObject jsonObject = new JSONObject(jsonArrPoints.getJSONObject(i).getString("owner"));
                        srm.setUserId(jsonObject.getInt("id"));
                        srm.setUserLogin(Html.fromHtml(jsonObject.getString("login")).toString());
                        srm.setUserImage(Html.fromHtml(jsonObject.getString("avatar_url")).toString());

                        ar.add(srm);
                    }
                    currentOPage++;
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
                error = true;
            } catch (Exception e) {
                e.printStackTrace();
                error = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if (error) {
                empty = true;
                error = false;
            }

            if (ar == null || ar.size() == 0) {
                loadMore = false;
                Repository srm = new Repository();
                srm.setId(1);
                ar.add(srm);

                empty = true;
            } else {

                if (jsonObjPoints.length() != 0) {
                    empty = false;
                    loadMore = true;
                } else {
                    empty = true;
                    loadMore = false;
                }

            }

            swipeRefreshLayout.setRefreshing(false);

            adapter.setLoading(false);
            carregando = false;
            adapter.Update(empty, pos);
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * This method is set recycler view develop
     */
    private void recyclerViewDevelop() {

        // improve performance if you know that changes in content
        // do not change the size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        linearlayoutManager = new LinearLayoutManager(getActivity());
        // use a linear layout manager
        recyclerView.setLayoutManager(linearlayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // First param is number of columns and second param is orientation i.e Vertical or Horizontal
//        StaggeredGridLayoutManager gridLayoutManager =
//                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        // Attach the layout manager to the recycler view
        //recyclerView.setLayoutManager(gridLayoutManager);

        // Create the recyclerViewAdapter
        adapter = new RepositoryAdapter(getActivity(), ar, empty, pos);
        recyclerView.setAdapter(adapter);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();

                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!carregando) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (linearlayoutManager.findLastVisibleItemPosition() > adapter.getItemCount() - 5 && loadMore && ultimoEstadoConectado) {

                        adapter.notifyDataSetChanged();
                        mountUrl();
                        progressBar.setVisibility(View.VISIBLE);

                        loadMore = false;
                    }
                }
            }
        });

        mountUrl();
    }
}