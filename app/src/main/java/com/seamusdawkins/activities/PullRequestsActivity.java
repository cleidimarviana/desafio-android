/*
*    The MIT License (MIT)
*
*   Copyright (c) 2016 Cleidimar Viana (cleidimarviana@gmail.com)
*
*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:
*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*/
package com.seamusdawkins.activities;

import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seamusdawkins.R;
import com.seamusdawkins.adapters.PullRequestAdapter;
import com.seamusdawkins.models.RepositoryResult;
import com.seamusdawkins.models.PullRequest;
import com.seamusdawkins.utils.AndroidUtils;
import com.seamusdawkins.utils.Consts;
import com.seamusdawkins.utils.HttpHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class PullRequestsActivity extends AppCompatActivity {

    private static String TAG = PullRequestsActivity.class.getSimpleName();
    private Toolbar toolbar;
    private int positions;

    private PullRequestAdapter adapter;

    private String urlPost;
    private JSONArray jsonObjPoints;

    private boolean broadcast;
    private AsyncCampaignParseJson myTask;

    private ArrayList<PullRequest> ar;
    private boolean loadMore = true;
    private boolean ultimoEstadoConectado = true;
    private Boolean error = false;
    private boolean empty = false;
    private int pos;
    private RecyclerView recyclerView;

    private LinearLayoutManager linearlayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;

    private boolean carregando = false;
    private IntentFilter filters;
    private int recyclerViewPaddingTop;

    private RepositoryResult habitResult;

    int contOpened = 0;
    int contClosed = 0;

    private TextView tvOpened;
    private TextView tvClosed;
    private LinearLayout llCount;

    private Animation animSlideDown, animSlideUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);
        Bundle extras = getIntent().getExtras();
        positions = extras.getInt("position");
        habitResult = (RepositoryResult) getIntent().getSerializableExtra("object");

        setToolbar(habitResult.ar.get(positions).getName());   // setting toolbar
        initUI();       // mapping xml interface with java
        initListener();

        // check connection with internet
        if (AndroidUtils.isConnected(this)) {
            Log.wtf(TAG, "Connection");
            recyclerViewDevelop();  // setting recycleview
            swipeToRefresh();       // setup swipe to refresh
        } else {
            Toast.makeText(this, getResources().getString(R.string.error_no_connection), Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
            recyclerViewDevelop();  // setting recycleview
            swipeToRefresh();       // setup swipe to refresh
        }

    }

    /**
     * This method to configure the toolbar.
     * Set a Toolbar to replace the ActionBar
     */
    public void setToolbar(String title) {
        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.include);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * This method maping xml interface with java.
     */
    public void initUI() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevelop);

        tvOpened = (TextView) findViewById(R.id.tvOpened);
        tvClosed = (TextView) findViewById(R.id.tvClosed);
        llCount = (LinearLayout) findViewById(R.id.llCount);
    }

    /**
     * This method implementing a listener
     */
    public void initListener() {

        animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /**
     * This method adjust url
     */
    private void mountUrl() {

        urlPost = Consts.URL_SERVER_DOMAIN + "repos/" + habitResult.ar.get(positions).getUserLogin() + "/" + habitResult.ar.get(positions).getName() + "/pulls";
        myTask = new AsyncCampaignParseJson();
        myTask.execute(urlPost);
    }


    public class AsyncCampaignParseJson extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            adapter.setLoading(true);
            carregando = true;

        }

        // get JSON Object
        @Override
        protected String doInBackground(String... url) {

            urlPost = url[0];
            try {

                jsonObjPoints = HttpHelper.doGetArray(PullRequestsActivity.this, urlPost, "UTF-8", "");

                if (broadcast) {
                    ar.clear();
                    broadcast = false;
                    adapter.UpdateArraySearch(ar);
                }

                if (ar == null) {
                    ar = new ArrayList<PullRequest>();
                    adapter.UpdateArraySearch(ar);
                }


                if (jsonObjPoints.length() != 0) {

                    for (int i = 0; i < jsonObjPoints.length(); i++) {

                        PullRequest srm = new PullRequest();

                        srm.setId(jsonObjPoints.getJSONObject(i).getInt("id"));
                        srm.setTitle(Html.fromHtml(jsonObjPoints.getJSONObject(i).getString("title")).toString());
                        srm.setBody(Html.fromHtml(jsonObjPoints.getJSONObject(i).getString("body")).toString());
                        srm.setHtmlURL(Html.fromHtml(jsonObjPoints.getJSONObject(i).getString("html_url")).toString());
                        srm.setCreatedAt(Html.fromHtml(jsonObjPoints.getJSONObject(i).getString("created_at")).toString());

                        if (jsonObjPoints.getJSONObject(i).getString("state").equals("open")) {
                            contOpened++;
                        }

                        JSONObject jsonObject = new JSONObject(jsonObjPoints.getJSONObject(i).getString("user"));
                        srm.setUserId(jsonObject.getInt("id"));
                        srm.setUserLogin(Html.fromHtml(jsonObject.getString("login")).toString());
                        srm.setUserImage(Html.fromHtml(jsonObject.getString("avatar_url")).toString());


                        ar.add(srm);
                    }

                    contClosed = jsonObjPoints.length() - contOpened;


                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
                error = true;
            } catch (Exception e) {
                e.printStackTrace();
                error = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if (error) {

                empty = true;
                error = false;
            }

            if (ar == null || ar.size() == 0) {
                loadMore = false;
                PullRequest srm = new PullRequest();
                srm.setId(1);
                ar.add(srm);

                empty = true;
            } else {

                if (jsonObjPoints.length() != 0) {
                    empty = false;
                    loadMore = true;
                } else {
                    empty = true;
                    loadMore = false;
                }

            }

            Log.wtf(TAG, "OPEN: " + contOpened);

            tvOpened.setText(contOpened + " opened");
            tvClosed.setText(" / " + contClosed + " closed");
            llCount.setVisibility(View.VISIBLE);
            llCount.startAnimation(animSlideDown);


            swipeRefreshLayout.setRefreshing(false);

            adapter.setLoading(false);
            carregando = false;
            adapter.Update(empty, pos);
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * This method is set recycler view develop
     */
    private void recyclerViewDevelop() {

        // improve performance if you know that changes in content
        // do not change the size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        linearlayoutManager = new LinearLayoutManager(this);
        // use a linear layout manager
        recyclerView.setLayoutManager(linearlayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // First param is number of columns and second param is orientation i.e Vertical or Horizontal
//        StaggeredGridLayoutManager gridLayoutManager =
//                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        // Attach the layout manager to the recycler view
        //recyclerView.setLayoutManager(gridLayoutManager);

        // Create the recyclerViewAdapter
        adapter = new PullRequestAdapter(this, ar, empty, pos);
        recyclerView.setAdapter(adapter);


        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();

                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!carregando) {
                    super.onScrolled(recyclerView, dx, dy);
//
                    if (linearlayoutManager.findLastVisibleItemPosition() > adapter.getItemCount() - 5 && loadMore && ultimoEstadoConectado) {

//                        adapter.notifyDataSetChanged();
//                        mountUrl(filter, "");
//                        progressBar.setVisibility(View.VISIBLE);
//
//                        loadMore = false;
                    }
                }
            }
        });

        mountUrl();

        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void swipeToRefresh() {
        int start = convertToPx(0), end = recyclerViewPaddingTop + convertToPx(16);
        swipeRefreshLayout.setProgressViewOffset(true, start, end);
        TypedValue typedValueColorPrimary = new TypedValue();
        TypedValue typedValueColorAccent = new TypedValue();
        this.getTheme().resolveAttribute(R.attr.colorPrimary, typedValueColorPrimary, true);
        this.getTheme().resolveAttribute(R.attr.colorAccent, typedValueColorAccent, true);
        final int colorPrimary = typedValueColorPrimary.data, colorAccent = typedValueColorAccent.data;
        swipeRefreshLayout.setColorSchemeColors(colorPrimary, colorAccent);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                contClosed = 0;
                contOpened = 0;

                llCount.setVisibility(View.GONE);
                llCount.startAnimation(animSlideUp);
                error = false;
                empty = false;
                myTask.cancel(true);
                ar.clear();
                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.INVISIBLE);
                mountUrl();


            }
        });
    }

    public int convertToPx(int dp) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (dp * scale + 0.5f);
    }
}
