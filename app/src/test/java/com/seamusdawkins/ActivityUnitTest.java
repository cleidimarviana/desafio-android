package com.seamusdawkins;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.ImageView;


public class ActivityUnitTest extends ActivityUnitTestCase<MainActivity> {
    public ActivityUnitTest() {
        super(MainActivity.class);
    }

    private MainActivity activity;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        Intent intent = new Intent(getInstrumentation().getTargetContext(), MainActivity.class);
        startActivity(intent, null, null);

        activity = getActivity();
    }

    public void testDevePossuirUmBotaoDeSubmit() throws Exception {

        ImageView submit = (ImageView)activity.findViewById(R.id.ivStars);

        assertNotNull(submit);
    }
}
