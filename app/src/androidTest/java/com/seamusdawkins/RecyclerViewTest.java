package com.seamusdawkins;

import android.test.ActivityInstrumentationTestCase2;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.seamusdawkins.TestUtils.withRecyclerView;


public class RecyclerViewTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public RecyclerViewTest() {
        super(MainActivity.class);
    }

    @Override protected void setUp() throws Exception {

        getActivity();
    }

    public void testItemClickPullRequest() throws InterruptedException {

        onView(withId(R.id.recyclerViewDevelop))
                                .perform(new TestUtils.ScrollToPositionViewAction(10));
        Thread.sleep(3000);
        onView(withId(R.id.recyclerViewDevelop))
                .perform(new TestUtils.ScrollToPositionViewAction(0));
        Thread.sleep(3000);
        onView(withRecyclerView(R.id.recyclerViewDevelop).atPosition(3)).perform(click());
        Thread.sleep(3000);
        onView(withRecyclerView(R.id.recyclerViewDevelop).atPosition(0)).perform(click());

    }
}
